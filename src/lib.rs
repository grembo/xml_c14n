#![doc = include_str!("../Readme.md")]

use libxml::bindings::{
    xmlC14NDocDumpMemory, xmlChar, xmlDocPtr, xmlFree, xmlFreeDoc, xmlNodeSet, xmlReadDoc,
};
use std::ffi::{c_int, c_void, CStr, CString};
use std::ptr::null;
use thiserror::Error;

/// Options for configuring how to canonicalize XML
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Default)]
pub struct CanonicalizationOptions {
    pub mode: CanonicalizationMode,
    pub keep_comments: bool,
}

/// Canonicalization specification to use
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Default)]
pub enum CanonicalizationMode {
    /// Original C14N 1.0 spec
    Canonical1_0,
    /// Exclusive C14N 1.0 spec
    #[default]
    ExclusiveCanonical1_0,
    /// C14N 1.1 spec
    Canonical1_1,
}

impl CanonicalizationMode {
    fn to_c_int(&self) -> c_int {
        c_int::from(match self {
            CanonicalizationMode::Canonical1_0 => 0,
            CanonicalizationMode::ExclusiveCanonical1_0 => 1,
            CanonicalizationMode::Canonical1_1 => 2,
        })
    }
}

/// An error code (always negative) returned by libxml2 when attempting to canonicalize some XML
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Default, Error)]
#[error("canonicalization error ({0})")]
pub struct CanonicalizationErrorCode(i32);

/// Parse specified XML document and canonicalize it
///
/// Example:
///
/// ```
/// use xml_c14n::{canonicalize_xml, CanonicalizationOptions, CanonicalizationMode};
///
/// let canonicalized = canonicalize_xml(
///     "<hi/>",
///     CanonicalizationOptions {
///         mode: CanonicalizationMode::Canonical1_0,
///         keep_comments: false,
///     }
/// ).unwrap();
///
/// assert_eq!(canonicalized, "<hi></hi>")
/// ```
pub fn canonicalize_xml(
    document: &str,
    options: CanonicalizationOptions,
) -> Result<String, CanonicalizationErrorCode> {
    // not sure how this works, but if XML is valid, this still succeeds, but canonicalize_document_to_c_pointer fails below.
    let document = read_document(document);

    unsafe {
        let (output, return_code) = canonicalize_document_to_c_pointer(options, document);

        let result = if return_code < 0 {
            Err(CanonicalizationErrorCode(return_code))
        } else {
            // SAFETY: xmlC14NDocDumpMemory completed successfully, so a proper C string was allocated and assigned to `output`
            let c_str = CStr::from_ptr(output as *const _);
            let str_slice: &str = c_str.to_str().unwrap();
            Ok(str_slice.to_owned())
        };

        let free_function = xmlFree.unwrap();
        free_function(output as *mut c_void);

        xmlFreeDoc(document);

        result
    }
}

/// Canonicalize document
///
/// If the operation completes successfully (return code is not negative), the returned pointer points to a valid C String
unsafe fn canonicalize_document_to_c_pointer(
    options: CanonicalizationOptions,
    document: xmlDocPtr,
) -> (*const xmlChar, c_int) {
    // "NULL if all document nodes should be included"
    let nodes = null::<xmlNodeSet>() as *mut _;
    let inclusive_ns_prefixes = null::<*mut xmlChar>() as *mut _;
    let with_comments = c_int::from(options.keep_comments);

    let mut output = null::<xmlChar>() as *mut xmlChar;

    let return_code = xmlC14NDocDumpMemory(
        document,
        nodes,
        options.mode.to_c_int(),
        inclusive_ns_prefixes,
        with_comments,
        (&mut output) as *mut _,
    );

    (output, return_code)
}

/// Parse the specified string to a [xmlDocPtr]
fn read_document(document: &str) -> xmlDocPtr {
    unsafe {
        let c_document = CString::new(document).unwrap();
        // TODO...
        let url = CString::default();

        // Encoding is allowed to be null as per docs
        let encoding = null();

        xmlReadDoc(
            // Pointer to null-terminated UTF8 required, which is what we pass here
            c_document.as_ptr() as *const xmlChar,
            url.as_ptr(),
            encoding,
            c_int::from(0),
        )
    }
}

#[cfg(test)]
mod tests {
    //! Test cases are taken from official spec and other sources. For more info see corresponding Readmes.
    use super::*;

    #[test]
    fn canonical_1_1_example_3_1_no_comment() {
        let input = include_str!("samples/canonical_1_1/3_1_input.xml");
        let expected = include_str!("samples/canonical_1_1/3_1_output_no_comment.xml");

        let canonicalized = canonicalize_xml(
            input,
            CanonicalizationOptions {
                mode: CanonicalizationMode::Canonical1_1,
                keep_comments: false,
            },
        )
        .unwrap();
        assert_eq!(canonicalized, expected)
    }

    #[test]
    fn canonical_1_1_example_3_1() {
        let input = include_str!("samples/canonical_1_1/3_1_input.xml");
        let expected = include_str!("samples/canonical_1_1/3_1_output.xml");

        let canonicalized = canonicalize_xml(
            input,
            CanonicalizationOptions {
                mode: CanonicalizationMode::Canonical1_1,
                keep_comments: true,
            },
        )
        .unwrap();
        assert_eq!(canonicalized, expected)
    }

    #[test]
    fn canonical_1_1_example_3_2() {
        let input = include_str!("samples/canonical_1_1/3_2_input.xml");
        let expected = include_str!("samples/canonical_1_1/3_2_output.xml");

        let canonicalized = canonicalize_xml(
            input,
            CanonicalizationOptions {
                mode: CanonicalizationMode::Canonical1_1,
                keep_comments: true,
            },
        )
        .unwrap();

        // for some reason, we get a stray \n at end of file :/
        assert_eq!(canonicalized, expected.trim())
    }

    #[test]
    fn canonical_exclusive_example_1() {
        let input = include_str!("samples/canonical_exclusive/1_input.xml");
        let expected = include_str!("samples/canonical_exclusive/1_output.xml");

        let canonicalized = canonicalize_xml(
            input,
            CanonicalizationOptions {
                mode: CanonicalizationMode::ExclusiveCanonical1_0,
                keep_comments: true,
            },
        )
        .unwrap();

        // for some reason, we get a stray \n at end of file :/
        assert_eq!(canonicalized, expected.trim())
    }

    #[test]
    fn invalid_xml() {
        let input = "<invalid xml";
        let canonicalized = canonicalize_xml(
            input,
            CanonicalizationOptions {
                mode: CanonicalizationMode::Canonical1_0,
                keep_comments: false,
            },
        );
        assert!(canonicalized.is_err())
    }

    #[test]
    fn display_error() {
        let formatted = format!("{}", CanonicalizationErrorCode(-1));
        let expected = "canonicalization error (-1)";
        assert_eq!(formatted, expected);
    }
}
